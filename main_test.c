int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */
  //注册调试接口
  key_board_debug_register(key_print_debug_callback);
  //初始化硬件及按键库
  GPIO_Key_Board_Init();
  //初始化使用的定时器
  HAL_InitTim2();
  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  
  /* USER CODE BEGIN 2 */
  PRINTF("start running\n");
  
    //注册组合状态测试
    struct key_combine_t test_combine1[] = {
        {
            .id = KEY_UP,
            .state = KEY_PRESS
        },
        {
            .id = KEY_DOWN,
            .state = KEY_PRESS_LONG
        },
        {
            .id = KEY_UP,
            .state = KEY_PRESS
        },
    };
    test_id1 = key_combine_register(test_combine1, GET_ARRAY_SIZE(test_combine1));

    struct key_combine_t test_combine2[] = {
        {
            .id = KEY_UP,
            .state = KEY_PRESS
        },
        {
            .id = KEY_DOWN,
            .state = KEY_PRESS
        },
        {
            .id = KEY_UP,
            .state = KEY_PRESS
        },
        {
            .id = KEY_DOWN,
            .state = KEY_PRESS
        },
    };
    test_id2 = key_combine_register(test_combine2, GET_ARRAY_SIZE(test_combine2));

    unsigned int res;
  /* USER CODE END 2 */
  
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
    if(key_check_state(KEY_UP, KEY_RELEASE))
    {
        PRINTF("KEY_UP KEY_RELEASE\r\n");
    }
    if(key_check_state(KEY_UP, KEY_PRESS))
    {
        PRINTF("KEY_UP KEY_PRESS\r\n");
    }
    if(key_check_state(KEY_UP, KEY_PRESS_LONG))
    {
        PRINTF("KEY_UP KEY_PRESS_LONG\r\n");
    }
    if(key_check_state(KEY_UP, KEY_RELEASE_LONG))
    {
        PRINTF("KEY_UP KEY_RELEASE_LONG\r\n");
    }
    res = key_check_state(KEY_UP, KEY_PRESS_MULTI);
    if(res)
    {
        PRINTF("KEY_UP KEY_PRESS_MULTI:%d\r\n", res);
    }
    res = key_check_state(KEY_UP, KEY_RELEASE_MULTI);
    if(res)
    {
        PRINTF("KEY_UP KEY_RELEASE_MULTI:%d\r\n", res);
    }
    if(key_check_state(KEY_UP, KEY_PRESS_CONTINUOUS))
    {
        PRINTF("KEY_UP KEY_PRESS_CONTINUOUS\r\n");
    }

    if(key_check_state(KEY_DOWN, KEY_RELEASE))
    {
        PRINTF("KEY_DOWN KEY_RELEASE\r\n");
    }
    if(key_check_state(KEY_DOWN, KEY_PRESS))
    {
        PRINTF("KEY_DOWN KEY_PRESS\r\n");
    }
    if(key_check_state(KEY_DOWN, KEY_PRESS_LONG))
    {
        PRINTF("KEY_DOWN KEY_PRESS_LONG\r\n");
    }
    if(key_check_state(KEY_DOWN, KEY_RELEASE_LONG))
    {
        PRINTF("KEY_DOWN KEY_RELEASE_LONG\r\n");
    }
    res = key_check_state(KEY_DOWN, KEY_PRESS_MULTI);
    if(res)
    {
        PRINTF("KEY_DOWN KEY_PRESS_MULTI:%d\r\n", res);
    }
    res = key_check_state(KEY_DOWN, KEY_RELEASE_MULTI);
    if(res)
    {
        PRINTF("KEY_DOWN KEY_RELEASE_MULTI:%d\r\n", res);
    }
    if(key_check_state(KEY_DOWN, KEY_PRESS_CONTINUOUS))
    {
        PRINTF("KEY_DOWN KEY_PRESS_CONTINUOUS\r\n");
    }

    if(key_check_combine_state(test_id1))
    {
        PRINTF("combine test_id1\r\n");
    }

    if(key_check_combine_state(test_id2))
    {
        PRINTF("combine test_id2\r\n");
    }
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}